//引入express框架
const express = require("express");

//创建路由对象
const home = express.Router();

// 获取菜品列表
home.post("/foods/getFoods", require("../controller/home/foods/getFoods"));
// 获取订单列表
home.post("/orders/getOrders", require("../controller/home/order/getOrders"));
// 添加未支付菜品订单
home.post("/orders/addOrders", require("../controller/home/order/addOrders"));
// 当菜品数量为零时删除菜品订单
home.delete("/orders/deleteOrder",require("../controller/home/order/deleteOrder"));
// 更新订单状态
home.put("/orders/updateOrders",require("../controller/home/order/updateOrders"));
// 充值余额
home.post("/personal/recargar",require("../controller/home/personal/recargar"));
// 修改密码
home.post("/personal/changePass",require("../controller/home/personal/changePass"));
// 获取菜品评论信息
home.post("/comment/getComments", require("../controller/home/comments/getCommnets"))
// 添加菜品评论
home.post("/comment/addComments", require("../controller/home/comments/addComments"))

module.exports = home;
