//引入express框架
const express = require('express');

//创建路由对象
const admin = express.Router();

// 获取左侧菜单列表
admin.get('/getMenuList', require('../controller/admin/getMenu'))

// 用户管理
admin.post('/user/getUserList', require('../controller/admin/user/getUserList'))
admin.post('/user/getUserById', require('../controller/admin/user/getUserById'))
admin.post('/user/addUser', require('../controller/regist'))
admin.put('/user/updateUser', require('../controller/admin/user/updateUser'))
admin.delete('/user/deleteUser',require('../controller/admin/user/deleteUser'))

// 权限管理
admin.get('/getRoleList', require('../controller/admin/getRole'))
admin.post('/rights/roles/addRole', require('../controller/admin/rights/roles/addRole'))
admin.put('/rights/roles/updateRole', require('../controller/admin/rights/roles/updateRole'))
admin.delete('/rights/roles/daleteRole', require('../controller/admin/rights/roles/deleteRole'))
admin.get('/getParamList', require('../controller/admin/getParamList'))
admin.post('/rights/params/addParam', require('../controller/admin/rights/params/addParam'))
admin.put('/rights/params/updateParam', require('../controller/admin/rights/params/updateParam'))
admin.delete('/rights/params/daleteParam', require('../controller/admin/rights/params/deleteParam'))

// 菜品管理
admin.post('/foods/getFoodList', require('../controller/admin/foods/getFoodList'))
admin.post('/foods/addFood', require('../controller/admin/foods/addFood'))
admin.delete('/foods/deleteFood', require('../controller/admin/foods/deleteFood'))
admin.put('/foods/updateFood', require('../controller/admin/foods/updateFood'))

// 餐桌管理
admin.post('/table/getTable', require('../controller/admin/table/getTable'))
admin.post('/table/addTable', require('../controller/admin/table/addTable'))
admin.put('/table/updateTable', require('../controller/admin/table/updateTable'))
admin.delete('/table/deleteTable', require('../controller/admin/table/deleteTable'))
admin.put('/table/choseTable',  require('../controller/admin/table/choseTable'))

// 订单管理
admin.post('/orderItem/getOrderItem', require('../controller/admin/order/getOrderItem'))
admin.post('/orderItem/getOrderInfo', require('../controller/admin/order/getOrderInfo'))
admin.delete('/orderItem/delete', require('../controller/admin/order/deleteOrder'))

// 评论管理
admin.post('/comments/getComments', require('../controller/admin/comments/getComment'))
admin.delete('/comments/deleteComments', require('../controller/admin/comments/deleteComments'))

// 营业统计
admin.get('/countData/addCount', require('../controller/admin/counts/addCount'))

// 將路由對象作为模块成员导出
module.exports = admin