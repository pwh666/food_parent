// 引入express框架
const express = require('express')
// 引入模块 body-parser 用来处理post请求参数
const bodyParser = require('body-parser')
//导入express-session模块
const session = require('express-session');
// 引入jwt 模块
const jwt = require('./api/jwt')
// 引入七牛云配置
const qnconfig = require('./qnconfig.js')
// 创建网站服务器
const app = express();
// 数据库连接
require('./model/connect.js')
//处理post请求参数
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }));
//配置session
app.use(session({
    secret: 'secret key',
    resave: true,
    saveUninitialized: false,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000
    }
}));

// 设置跨域和相应数据格式
app.all('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, mytoken')
    res.header('Access-Control-Allow-Headers', 'X-Requested-With, Authorization')
    res.setHeader('Content-Type', 'application/json;charset=utf-8')
    res.header('Access-Control-Allow-Headers', 'Content-Type,Content-Length, Authorization, Accept,X-Requested-With')
    res.header('Access-Control-Allow-Methods', 'PUT,POST,GET,DELETE,OPTIONS')
    res.header('X-Powered-By', ' 3.2.1')
    if (req.method == 'OPTIONS') res.sendStatus(200)
    /*让options请求快速返回*/ else next()
  })

//实现登录功能
app.post('/login', require('./controller/login'));
//实现注册功能
app.post('/regist', require('./controller/regist'));
app.get('/up/token', (req,res,next) => {
    return res.status(200).send(qnconfig.uploadToken)
})
//为路由匹配请求路径
app.use('/home', require('./route/home'));
app.use('/admin', require('./route/admin'));



/**
 *
 * 统一处理无响应
 *
 */
// 如果没有路径处理就返回 Not Found
app.use(function(req, res, next) {
    res.status(404).send('Not Found')
  })

// 监听端口
app.listen(3000, () => {
    console.log('网站服务器链接成功')
});
