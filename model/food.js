// 创建菜品集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建菜品集合规则
const foodSchema = new mongoose.Schema({
    foodName:{
        type: String,
        required: true
    },
    foodPrice: {
        type: Number,
        required: true
    },
    foodCost: {
      type: Number,
      required: true
    },
    foodClass: {
        type: [Object],
        required: true
    },
    foodPic:{
        type: String,
        default: null
    },
    foodDesc:{
        type: String,
        required: true
    }
})

// 创建集合
const Food = mongoose.model('Food', foodSchema)

// 生成菜品
function createFood() {
    Food.create({
        foodName: '香菇酱肉包',
        foodPrice: 2.5,
        foodCost: 1,
        foodClass: [{label: '早餐', value: 'breakfast'},{label: '主食', value: 'staples'}],
        foodPic: '',
        foodDesc: '香菇与酱肉的完美结合'
    })
}
// createFood()

module.exports = {
    Food
}