// 创建已支付订单集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建已支付订单集合规则
const orderSchema = new mongoose.Schema({
    uid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    fls: [{ // 菜品列表
        oid: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'orderItem'
        }
    }],
})

// 创建集合
const Order = mongoose.model('Order', orderSchema)
const ObjectId = mongoose.mongo.ObjectID

module.exports = {
    Order,
    ObjectId
}