// 创建评论集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建营业统计集合规则
const countSchema = new mongoose.Schema({
    countExpend: {
        type: Number,
        default: 0,
        required: true
    },
    countIncome: {
        type: Number,
        default: 0,
        required: true
    },
    countProfit: { 
        type: Number,
        default: 0,
        required: true
    },
    countTime: {
        type: String,
        unique: true,
        required: true
    }

})

// 创建集合
const Count = mongoose.model('count', countSchema)
// 生成营业统计
function createCount() {
    orderItem.create({
        countExpend: 150,
        countIncome: 100,
        countProfit: 50,
        countTime: '2022-05-05 15:03'
    })
}
// createOrder()

module.exports = {
    Count
}