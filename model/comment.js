// 创建评论集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建评论集合规则
const commentSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    foodId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Food'
    },
    content: {
        type: String
    },
    comTime: {
      type: String
    },
    satrNum: { 
        type: Number,
        default: 0,
        required: true
    }
})

// 创建集合
const Comments = mongoose.model('Comments', commentSchema)
// 生成评论
function createCom() {
  Comments.create({
        userId: '61b9873ac1e0042eef4c2f64',
        foodId: '61d654ceff97fb0b8874faf9',
        content: '米饭太硬，下次可以煮软一点',
        comTime: '2022-05-13 14:15:36',
        satrNum: 2
    })
}
// createCom()
const ObjectId = mongoose.mongo.ObjectID

module.exports = {
    Comments,
    ObjectId
}