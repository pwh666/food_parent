// 创建角色集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建角色集合规则
const roleSchema = new mongoose.Schema({
    label:{
        type: String,
        required: true
    },
    value: {
        type: String,
        unique: true, // 确保角色值唯一
        required: true
    }
})

// 创建集合
const Role = mongoose.model('Role', roleSchema)

// 生成角色
function createRole() {
    Role.create({
        label: '超级管理员',
        value: 'root'
    })
}
// createRole()

module.exports = {
    Role
}