// 创建已点菜品集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建已点菜品集合规则
const orderItemSchema = new mongoose.Schema({
    uid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    fid: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Food'
    },
    fnum: { //菜品数量
        type: Number,
        default: 1,
        required: true
    },
    orderNum: {
        type: String
    },
    orderTime: {
      type: String
    },
    tableNum: {
      type: String
    },
    isPay: {
        type: Boolean,
        required: true
    },
    orderState: { 
    /*
    订单状态
    0：未支付
    1：已支付
    2：正在退餐
    3：已退餐
    */ 
        type: Number,
        default: 0,
        required: true
    }
})

// 创建集合
const orderItem = mongoose.model('orderItem', orderItemSchema)
// 生成订单
function createOrder() {
    orderItem.create({
        uid: '61ab3fcdef373db277ae9044',
        fid: '61c92615d5716836aad07a21',
        fnum: 1,
        orderNum: '',
        orderTime: '',
        tableNum: '',
        isPay: false,
        orderState: 0
    })
}
// createOrder()
const ObjectId = mongoose.mongo.ObjectID

module.exports = {
    orderItem,
    ObjectId
}