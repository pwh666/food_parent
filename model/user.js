// 创建用户集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');
//导入bcrypt模块
const bcrypt = require('bcrypt');
// 导入joi验证模块
const Joi = require('joi')

// 创建用户集合规则
const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        // unique: true, // 用户名不重复
        minlength: 2,
        maxlength: 10
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String, // root 表示超级管理员 admin 表示管理员
        required: true
    },
    tel: {
        type: String, 
        required: true,
        unique: true //保证添加用户时手机号不重复
    },
    email: {
        type: String, 
        required: true
    },
    balance: {
        type: Number
    },
    recargaTotal: {
        type: Number
    }
})

// 创建集合
const User = mongoose.model('User', userSchema);

//创建用户
async function createUser() {
    // 生成随机字符串
    let salt = await bcrypt.genSalt(10);
    let pass = await bcrypt.hash('root', salt);
    User.create({
        username: 'root',
        password: pass,
        role: 'root',
        tel: '13551625948',
        email: 'root@163.com'
    })
}
// createUser();

//验证用户信息
const validateUser = (user) => {
    //获取传递过来的参数  req.body
    //定义对象的验证规则
    const schema = {
        username: Joi.string().min(2).max(10).required().error(new Error('用户名不符合规则')),
        tel: Joi.string().regex(/^(13[0-9]|14[5|7]|15[0|1|2|3|4|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/).required().error(new Error('手机号码格式不符合规则')),
        password: Joi.string().regex(/^[a-zA-Z0-9]{6,15}$/).required().error(new Error('密码格式不正确')),
        role: Joi.string().valid('root','admin', 'normal', 'VIP', 'SVIP', 'ZVIP').required().error(new Error('角色不存在')),
        email: Joi.string().email()
    };
    //实施验证
    return Joi.validate(user, schema);
};

module.exports = {
    User,
    validateUser
}