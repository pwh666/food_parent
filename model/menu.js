// 创建左侧导航栏集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建左侧导航栏集合规则
const menuSchema = new mongoose.Schema({
    menu_id: {
        type: Number,
        required: true,
    },
    menu_name: {
        type: String,
        required: true,
    },
    menu_pid: {
        type: Number, 
        required: true,
    },
    menu_level: {
        type: String, // 1 表示一级菜单，2 表示二级菜单
        required: true
    },
    menu_path: {
        type: String,
        required: true,
    },
    menu_order: {
        type: Number
    },
})

// 创建集合
const Menu = mongoose.model('Menu', menuSchema);

//创建左侧导航栏
async function createMenu() {
    Menu.create({
        menu_id: 104,
        menu_name: '菜品列表',
        menu_pid: 0,
        menu_level: 2,
        menu_path: 'goods',
        menu_order: 3
    })
}
// createMenu();

module.exports = {
    Menu
}