// 创建角色集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建角色集合规则
const foodClassSchema = new mongoose.Schema({
    label:{
        type: String,
        required: true
    },
    value: {
        type: String,
        unique: true, // 确保分类唯一
        required: true
    },
    order: {
        type: Number,
    }
})

// 创建集合
const FoodClass = mongoose.model('FoodClass', foodClassSchema)

// 生成角色
function createClass() {
    FoodClass.create({
        label: '特色菜',
        value: 'Special',
        order: 1
    })
}
// createClass()

module.exports = {
    FoodClass
}