// 创建餐桌集合
// 引入mongoose第三方模块
const mongoose = require('mongoose');

// 创建餐桌集合规则
const tableSchema = new mongoose.Schema({
    label:{
        type: String,
        required: true
    },
    number: {
        type: String,
        unique: true, 
        required: true
    },
    state: {
        /**餐桌状态
         * 1 表示空闲中
         * 2 表示点餐中
         * 3 表示用餐中
         * 4 表示已预定
         */
        type: String
    }
})

// 创建集合
const Table = mongoose.model('Table', tableSchema)

// 生成餐桌
function createTable() {
    Table.create({
        label: '1号桌',
        number: '1',
        state: '1'
    })
}
// createTable()

module.exports = {
    Table
}