//导入bcrypt模块
const bcrypt = require('bcrypt');
//导入 jwt 生成 token 令牌
const jwt = require('../api/jwt');
//导入用户集合构造函数
const { User } = require('../model/user');

module.exports = async (req, res) => {
    //接收请求参数
    const { username, password } = req.body;
    //如果用户没有输入用户名或密码
    if (username.trim().length == 0 || password.trim().length == 0) {
        return res.status(401).send({ msg: '请输入用户名或密码' })
    }
    //根据用户名查询用户
    //如果查询到了用户 user变量的值是对象类型 对象中存储的是用户信息
    //如果没有查询到用户 user变量为空
    let user = await User.findOne({ username });
    //查询到了用户
    if (user) {
        //将客户端传递过来的密码与用户信息中的密码进行比对，返回true或false
        let isEqual = await bcrypt.compare(password, user.password);
        if (isEqual) {
            let token = jwt.createToken({ username: username, password: password })
                return res.status(200).send({
                    token: token,
                    userInfo: user,
                    meta: {
                        success: "true", 
                        msg: "登录成功" 
                    }
                })
        } else {
            return res.status(401).send({ msg: '密码错误，请重新输入' })
        }
    } else {
        return res.status(401).send({ msg: '用户名不存在，请注册' })
    }
}