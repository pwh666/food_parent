// 导入菜品集合构造函数
const { Food } = require('../../../model/food')
const { FoodClass } = require('../../../model/foodClass')

module.exports = async(req, res) => {
    // 查询菜品分系
    let foodclass = await FoodClass.find({"value": {"$in": req.body.foodClass}})
    req.body.foodClass = foodclass
    // 更新菜品
    Food.findOneAndUpdate({_id: req.body._id}, req.body).then(() => {
        return res.status(200).send({success: 'true', msg: '更新菜品信息成功'})
    }).catch(err => {
        return JSON.stringify({message: err.message})
    })
}