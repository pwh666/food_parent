// 导入菜品集合构造函数
const { Food } = require('../../../model/food.js')
const { FoodClass } = require('../../../model/foodClass.js')


module.exports = async(req, res, next) => {
    // 查询该菜品是否已存在
    let foodInfo = await Food.findOne({foodName: req.body.foodName})
    if(foodInfo){
        return next(JSON.stringify({success: 'false', msg: '该菜品已存在'}))
    }
    let foodclass = await FoodClass.find({"value": {"$in": req.body.foodClass}})
    req.body.foodClass = foodclass
    // 添加菜品
    Food.create(req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
        return next(JSON.stringify({ message: err.message }))
    })
}