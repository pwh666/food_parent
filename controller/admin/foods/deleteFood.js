// 导入菜品集合构造函数
const { Food } = require('../../../model/food')

module.exports = (req, res) => {
    Food.findByIdAndDelete(req.body.id).then(() => {
        return res.status(200).send({success: 'true', msg: '删除成功'})
    }).catch(err => {
        return JSON.stringify({msg: err.msg})
    })
}