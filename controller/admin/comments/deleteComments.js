// 导入评论集合构造函数
const { Comments } = require('../../../model/comment')

module.exports = (req, res) => {
  Comments.findByIdAndDelete({_id: req.body.id}).then(() => {
        return res.status(200).send({success: 'true', msg: '删除成功'})
    }).catch(err => {
        return JSON.stringify({message: err.message})
    })
}