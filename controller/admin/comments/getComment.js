// 导入评论集合构造函数
const { json } = require('body-parser');
const { Comments } = require('../../../model/comment')

module.exports = async (req, res) => {
    //接收客户端传递过来的当前页参数
    let pagenum = req.body.pagenum || 1;
    //每一页显示的数据条数
    let pagesize = req.body.pagesize;
    //查询用户数据的总数
    let count = await Comments.countDocuments({});
    //总页数 Math.ceil() 向上取整
    // let total = Math.ceil(count / pagesize);
    //页码对应的数据查询开始位置
    let start = (pagenum - 1) * pagesize;

    //将用户信息从数据库中查询出来
    if(req.body.query !== '') {
        var comments = await Comments.find({username: req.body.query}).populate('userId').populate('foodId').limit(pagesize).skip(start).exec();
    }else{
      comments = await Comments.find().populate('userId').populate('foodId').limit(pagesize).skip(start).exec();
    }
    if (comments) {
        return res.status(200).send({ comments: comments, total: count, meta: { success: "true", msg: "获取评论列表成功" } })
    } else {
        return res.status(500).send({
            data: null,
            meta: {
                success: "false",
                msg: "获取评论列表失败"
            }
        })
    }
}