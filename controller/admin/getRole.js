// 导入角色集合构造函数
const { Role } = require('../../model/role.js')

// 获取角色列表
module.exports = async(req, res) => {
    let roleList = await Role.find();
    if(roleList) {
        return res.status(200).send({
            data: roleList,
            meta: {
                success: 'true',
                msg: '获取角色成功'
            }
        })
    }else{
        return res.status(500).send({
            data: null,
            meta: {
                success: 'false',
                msg: '获取角色失败'
            }
        })
    }
}