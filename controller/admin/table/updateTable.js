// 导入餐桌集合构造函数
const { Table } = require('../../../model/table')

module.exports = (req, res) => {
    Table.findByIdAndUpdate({_id: req.body._id}, req.body).then(() => {
        return res.status(200).send({success: 'true', msg: '更新成功'})
    }).catch(err => {
        return JSON.stringify({message: err.message})
    })
}