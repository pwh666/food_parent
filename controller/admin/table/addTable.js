// 导入餐桌集合构造函数
const { Table } = require('../../../model/table')

module.exports = async(req, res, next) => {
    let table = await Table.findOne({number: req.body.number})
    if(table){
        return res.status(501).send({ success: 'false', msg: '该餐桌已存在' })
    }
    // 添加餐桌
    Table.create(req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
        return next(JSON.stringify({ message: err.message }))
    })
}