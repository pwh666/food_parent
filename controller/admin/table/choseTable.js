// 导入餐桌集合构造函数
const { Table } = require('../../../model/table')

module.exports = (req, res) => {
    Table.findOneAndUpdate({number: req.body.number}, {$set: {'state': req.body.state}}).then(() => {
        return res.status(200).send({success: 'true', msg: '选择餐桌成功'})
    }).catch(err => {
        return JSON.stringify({message: err.message})
    })
}