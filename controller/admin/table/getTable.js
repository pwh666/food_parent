// 导入餐桌集合构造函数
const { Table } = require('../../../model/table')
// 引入 lodash 工具库
const _ = require('lodash')

module.exports = async(req, res) => {
    //接收客户端传递过来的当前页参数
    let pagenum = req.body.pagenum || 1;
    //每一页显示的数据条数
    let pagesize = req.body.pagesize;
    //查询餐桌数据的总数
    let count = await Table.countDocuments({});
    //总页数 Math.ceil() 向上取整
    // let total = Math.ceil(count / pagesize);
    //页码对应的数据查询开始位置
    let start = (pagenum - 1) * pagesize;
    Table.find().limit(pagesize).skip(start).then(response => {
        if (response) {
            // 排序
            let results = _.values(response)
            results = _.sortBy(response, "number")
            return res.status(200).send({
                data: results,
                total: count,
                meta: {
                    success: true,
                    msg: '获取餐桌成功'
                }
            })
        } else {
            return false
        }
    }).catch(err => {
        return err
    })
}