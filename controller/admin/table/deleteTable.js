// 导入餐桌集合构造函数
const {Table} = require('../../../model/table')

module.exports = (req, res) => {
    Table.findByIdAndDelete({_id: req.body.id}).then(() => {
        return res.status(200).send({success: 'true', msg: '删除成功'})
    }).catch(err => {
        return JSON.stringify({message: err.message})
    })
}