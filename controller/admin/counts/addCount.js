// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')
// 导入营业统计集合构造函数
const { Count } = require('../../../model/count');
const { ordered } = require('joi/lib/types/array');

module.exports = async (req, res) => {
    let orders = await orderItem.find({ orderState: 1 }).populate('fid').exec();
    const arr = []
    const result = []
    const obj = {}
    orders.forEach((item, index) => {
        if (!obj[item.orderTime]) {
            result.push({ orderTime: item.orderTime.substring(0, 10), child: [item] })
            obj[item.orderTime] = true
        } else {
            const curIndex = result.findIndex(val => val.orderTime.substring(0, 10) === item.orderTime.substring(0, 10))
            result[curIndex].child.push(item)
        }
    })
    for(let i = 0; i < result.length; i++){
        let incomeSum = 0, expendSum = 0, profitSum = 0, time = ''
        for(j = 0; j < result[i].child.length; j++){
            incomeSum += result[i].child[j].fnum * result[i].child[j].fid.foodPrice
            expendSum += result[i].child[j].fnum * result[i].child[j].fid.foodCost
        }
        profitSum = incomeSum - expendSum
        time = result[i].orderTime
        arr.push({
            countIncome: incomeSum,
            countExpend: expendSum,
            countProfit: profitSum,
            countTime: time
        })
    }
    Count.insertMany(arr, { ordered: false }, response => {
        return res.status(200).send({ countData: response, meta: { success: 'true', msg: '统计成功' } })
    })
}