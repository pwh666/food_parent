// 导入角色集合构造函数
const { Role } = require ('../../../../model/role')

module.exports = async(req, res, next) => {
    let roler = await Role.findOne({_id: req.body._id})
    if(roler){
        return next(JSON.stringify({success: 'false', msg: '该角色已存在'}))
    }
    Role.create(req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
        return next(JSON.stringify({ message: err.message }))
    })
}