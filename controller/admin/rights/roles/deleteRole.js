// 导入角色集合构造函数
const { Role } = require('../../../../model/role')

module.exports = (req, res) => {
    Role.findByIdAndDelete(req.body.id).then(() => {
        return res.status(200).send({success: 'true', msg: '删除成功'})
    }).catch(err => {
        return JSON.stringify({msg: err.msg})
    })
}