// 导入角色集合构造函数
const { Role } = require('../../../../model/role')

module.exports = (req, res) => {
    Role.findOneAndUpdate({_id: req.body._id}, req.body).then(() => {
        return res.status(200).send({success: 'true', msg: '修改角色信息成功'})
    }).catch(err => {
        return JSON.stringify({message: err.message})
    })
}