// 导入菜系集合构造函数
const { FoodClass } = require ('../../../../model/foodClass')

module.exports = async(req, res, next) => {
    let param = await FoodClass.findOne({value: req.body.value})
    if(param){
        return next(JSON.stringify({success: 'false', msg: '该菜系已存在'}))
    }
    FoodClass.create(req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
        return next(JSON.stringify({ message: err.message }))
    })
}