// 导入菜系集合构造函数
const { FoodClass } = require ('../../../../model/foodClass')

module.exports = (req, res) => {
    FoodClass.findByIdAndDelete(req.body.id).then(() => {
        return res.status(200).send({success: 'true', msg: '删除成功'})
    }).catch(err => {
        return JSON.stringify({msg: err.msg})
    })
}