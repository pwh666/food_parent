// 引入 lodash 工具库
const _ = require('lodash')

// 导入菜单集合构造函数
const { Menu } = require('../../model/menu.js')

module.exports = async(req, res, next) => {
    let menus = await Menu.find({})
    if(menus) {
        let menuList = {}
        // 处理一级菜单
        for(idx in menus) {
            let m = menus[idx]
            if(m.menu_level == 1) {
                menuList[m.menu_id] = {
                    "id":m.menu_id,
                    "authName":m.menu_name,
                    "path":m.menu_path,
                    "children":[],
                    "order": m.menu_order
                };
            }
        }
        // 处理二级菜单
        for(idx in menus) {
            let m = menus[idx]
            if(m.menu_level == 2) {
                if(menuList[m.menu_pid]){
                    menuList[m.menu_pid].children.push({
                        "id":m.menu_id,
                        "authName":m.menu_name,
                        "path":m.menu_path,
                        "children":[],
                        "order": m.menu_order
                    });
                }
            }
        }
        // 排序
        let result = _.values(menuList)
        result = _.sortBy(menuList, "order")
        for(idx in result) {
            subresult = result[idx];
            subresult.children = _.sortBy(subresult.children,"order");
        }
        return res.status(200).send({
            data: result, 
            meta: {
                success: "true", 
                msg: "获取菜单列表成功" 
            }
        })
    }
    return res.status(501).send({
        data: null, 
        meta: {
            success: "false", 
            msg: "获取菜单列表失败" 
        }
    })
}