// 导入用户集合构造函数
const { User } = require('../../../model/user.js')

module.exports = async (req, res) => {
    //将用户信息从数据库中查询出来
    let userInfo = await User.findById(req.body.id);
    if (userInfo) {
        return res.status(200).send({ userInfo: userInfo, meta: { success: "true", msg: "获取用户信息成功" } })
    } else {
        return res.status(500).send({
            data: null,
            meta: {
                success: "false",
                msg: "获取用户失信息失败"
            }
        })
    }
}