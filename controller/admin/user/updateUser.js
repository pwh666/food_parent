// 导入用户集合构造函数
const { User } = require('../../../model/user.js')
// 引入加密模块bcrypt
const bcrypt = require('bcrypt');

module.exports = async(req, res) => {
    if (req.body.password && req.body.password !== '') {
        // 对密码进行加密处理 生成随机字符串
        let salt = await bcrypt.genSalt(10);
        // 密码加密
        let saltpw = await bcrypt.hash(req.body.password, salt);
        // 密码替换
        req.body.password = saltpw;
    }
    User.findByIdAndUpdate(req.body._id, req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '更新成功' })
    }).catch(err => {
        return err
    })
}