// 导入用户集合构造函数
const { User } = require('../../../model/user.js')

module.exports = (req, res) => {
    User.findByIdAndDelete(req.body.id).then(() => {
        return res.status(200).send({success: 'true', msg: '删除成功'})
    }).catch(err => {
        return JSON.stringify({msg: err.msg})
    })
}