// 导入用户集合构造函数
const { json } = require('body-parser');
const { User } = require('../../../model/user.js')

module.exports = async (req, res) => {
    //接收客户端传递过来的当前页参数
    let pagenum = req.body.pagenum || 1;
    //每一页显示的数据条数
    let pagesize = req.body.pagesize;
    //查询用户数据的总数
    let count = await User.countDocuments({});
    //总页数 Math.ceil() 向上取整
    // let total = Math.ceil(count / pagesize);
    //页码对应的数据查询开始位置
    let start = (pagenum - 1) * pagesize;

    //将用户信息从数据库中查询出来
    if(req.body.query !== '') {
        var users = await User.find({username: req.body.query}).limit(pagesize).skip(start);
    }else{
        users = await User.find().limit(pagesize).skip(start);
    }
    if (users) {
        return res.status(200).send({ users: users, total: count, meta: { success: "true", msg: "获取用户列表成功" } })
    } else {
        return res.status(500).send({
            data: null,
            meta: {
                success: "false",
                msg: "获取用户列表失败"
            }
        })
    }
}