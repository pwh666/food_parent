// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')

module.exports = async (req, res) => {
  let orderNum = req.body.orderNum
  //将订单从数据库中查询出来
  let orderItems = await orderItem.find({orderNum: orderNum}).populate('fid').populate('uid').exec();
  if (orderItems) {
    return res.status(200).send({ data: orderItems, meta: { success: "true", msg: "获取订单成功" } })
  } else {
    return res.status(500).send({
      data: null,
      meta: {
        success: "false",
        msg: "获取订单失败"
      }
    })
  }
}