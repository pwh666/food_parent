// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')

module.exports = async (req, res) => {
  //接收客户端传递过来的当前页参数
  let pagenum = req.body.pagenum || 1;
  //每一页显示的数据条数
  let pagesize = req.body.pagesize;
  //查询订单总数
  let count = await orderItem.countDocuments();
  //总页数 Math.ceil() 向上取整
  // let total = Math.ceil(count / pagesize);
  //页码对应的数据查询开始位置
  let start = (pagenum - 1) * pagesize;

  //将订单从数据库中查询出来
  if (req.body.query !== '') {
    var orderItems = await orderItem.find({ orderNum: req.body.query }).populate('uid').populate('fid').limit(pagesize).skip(start).exec();
    count = await orderItem.countDocuments({orderNum: req.body.query});
  } else {
    orderItems = await orderItem.find({}).populate('uid').populate('fid').limit(pagesize).skip(start).exec();
  }
  if (orderItems) {
    //过滤得到已支付的菜品
    let toPayArr = orderItems.filter(item => { return item.isPay == true });
    let str = JSON.stringify(toPayArr);
    let json = JSON.parse(str)
    return res.status(200).send({ orderItems: json, total: count, meta: { success: "true", msg: "获取订单列表成功" } })
  } else {
    return res.status(500).send({
      data: null,
      meta: {
        success: "false",
        msg: "获取订单列表失败"
      }
    })
  }
}