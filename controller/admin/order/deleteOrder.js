// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')

module.exports = (req, res) => {
  orderItem.remove({orderNum: req.body.orderNum}).then(() => {
      return res.status(200).send({success: 'true', msg: '删除成功'})
  }).catch(err => {
      return JSON.stringify({message: err.message})
  })
}