// 导入菜品集合构造函数
const { FoodClass } = require('../../model/foodClass')
// 引入 lodash 工具库
const _ = require('lodash')

module.exports = (req, res) => {
    FoodClass.find().then(result => {
        if (result) {
            // 排序
            let results = _.values(result)
            results = _.sortBy(result, "order")
            return res.status(200).send({
                data: results,
                meta: {
                    success: 'true',
                    msg: '获取菜系成功'
                }
            })
        }
    }).catch(err => {
        return JSON.stringify({ message: err.message })
    })
}