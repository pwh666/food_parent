// 导入用户集合构造函数
const { User } = require('../../../model/user.js')

module.exports = async (req, res) => {
    User.findOneAndUpdate({ _id: req.body.userId }, { $inc: { 'balance': req.body.balance, 'recargaTotal': req.body.balance } }).then((response) => {
        if (response.role !== 'root' && response.role !== 'admin') {
            switch (response.role) {
                case 'normal':
                    User.findOneAndUpdate({ _id: req.body.userId }, { $set: { 'role': 'VIP' } }).then(() => {
                        return res.status(200).send({ success: true, msg: '充值成功' })
                    })
                    break
                case 'VIP':
                    if (response.recargaTotal >= 70) {
                        User.findOneAndUpdate({ _id: req.body.userId }, { $set: { 'role': 'SVIP' } })
                    }
                    return res.status(200).send({ success: true, msg: '充值成功' })
                case 'SVIP':
                    if (response.recargaTotal >= 170) {
                        User.findOneAndUpdate({ _id: req.body.userId }, { $set: { 'role': 'ZVIP' } })
                   }
                   return res.status(200).send({ success: true, msg: '充值成功' })
                case 'ZVIP':
                    if (response.recargaTotal >= 370) {
                        User.findOneAndUpdate({ _id: req.body.userId }, { $set: { 'role': 'GVIP' } })
                    }
                    return res.status(200).send({ success: true, msg: '充值成功' })
                default:
                    return res.status(200).send({ success: true, msg: '充值成功' })
            }
        } else {
            return res.status(200).send({ success: true, msg: '充值成功' })
        }
    }).catch(err => {
        return JSON.parse(JSON.stringify(err))
    })
}