// 导入用户集合构造函数
const { User } = require("../../../model/user.js");
//导入bcrypt模块
const bcrypt = require("bcrypt");

module.exports = async (req, res) => {
  let user = User.findById(req.body.id);
  if (user) {
    let isEqual = await bcrypt.compare(req.body.oldPassword, user.password);
    if (isEqual) {
      // 对密码进行加密处理 生成随机字符串
      let salt = await bcrypt.genSalt(10);
      // 密码加密
      let saltpw = await bcrypt.hash(req.body.newPassword, salt);
      // 密码替换
      req.body.newPassword = saltpw;
      User.updateOne(
        {
          _id: req.body.id,
        },
        {
          $set: {
            password: req.body.newPassword,
          },
        }
      )
        .then(() => {
          return res.status(200).send({ success: "true", msg: "修改密码成功" });
        })
        .catch((err) => {
          return JSON.parse(JSON.stringify(err));
        });
    } else {
      return res
        .status(501)
        .send({ success: "false", msg: "旧密码错误，请重新输入" });
    }
  }
};
