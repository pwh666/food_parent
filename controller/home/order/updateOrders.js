// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')
// 导入餐桌集合构造函数
const { Table } = require('../../../model/table')

module.exports = async (req, res) => {
  // 更新订单状态
  orderItem.updateMany({
    _id: { '$in': req.body.ids }
  }, {
    $set: {
      'orderNum': req.body.orderNum,
      'tableNum': req.body.tableNum,
      'orderTime': req.body.orderTime,
      'isPay': true,
      'orderState': 1
    }
  }).then(() => {
    Table.findOneAndUpdate({number: req.body.tableNum}, {$set: {'state': '3'}}).then(() => {
      return res.status(200).send({ success: 'true', msg: '下单支付成功' })
    })
  }).catch(err => {
    return JSON.parse(JSON.stringify(err))
  })
}