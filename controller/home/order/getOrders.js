// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')

module.exports = async (req, res) => {
  let user = req.body.userId
  // 判断已支付订单或未支付订单
  let isPayed = req.body.isPayed
  //将订单从数据库中查询出来
  let orderItems = await orderItem.find({uid: user}).populate('fid').exec();
  if (orderItems) {
    //过滤得到未支付或已支付的订单
    let toPayArr = orderItems.filter((item, index) => { return item.isPay == isPayed });
    let str = JSON.stringify(toPayArr);
    let json = JSON.parse(str)
    return res.status(200).send({ data: json, total: toPayArr.length, meta: { success: "true", msg: "获取订单列表成功" } })
  } else {
    return res.status(500).send({
      data: null,
      meta: {
        success: "false",
        msg: "获取订单列表失败"
      }
    })
  }
}