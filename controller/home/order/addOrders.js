// 导入订单集合构造函数
const { orderItem } = require('../../../model/orderItem.js')

module.exports = async (req, res) => {
  let food = req.body.fid
  let user = req.body.uid

  // 查询该菜品是否已存在
  let orderItems = await orderItem.findOne({ fid: food, uid: user, isPay: false });
  // 如果已存在，则更新已点菜品数量
  if (orderItems) {
    orderItem.findOneAndUpdate({ fid: food, uid: user, isPay: false }, { $inc: { 'fnum': 1 } }).then(() => {
      return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
      return JSON.stringify({message: err.message})
    })
  } else {
    // 如果不存在，则新增菜品
    orderItem.create(req.body).then(() => {
      return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
      return JSON.stringify({message: err.message})
    })
  }
}