// 导入菜品集合构造函数
const { Food } = require('../../../model/food.js')

module.exports = async (req, res) => {
    //接收客户端传递过来的当前页参数
    let pagenum = req.body.pagenum || 1;
    //每一页显示的数据条数
    let pagesize = req.body.pagesize;
    //查询菜品数据的总数
    let counts = await Food.countDocuments({});
    //总页数 Math.ceil() 向上取整
    // let total = Math.ceil(count / pagesize);
    //页码对应的数据查询开始位置
    let start = (pagenum - 1) * pagesize;

    //将菜品信息从数据库中查询出来
    if (req.body.query !== '') {
        var food = await Food.find({ foodClass: {$elemMatch: { value: req.body.query }} }).limit(pagesize).skip(start);
        counts = await Food.countDocuments({ foodClass: {$elemMatch: { value: req.body.query }} })
      } else {
        food = await Food.find().limit(pagesize).skip(start);
    }
    if (food) {
        return res.status(200).send({ food: food, total: counts, meta: { success: "true", msg: "获取菜品列表成功" } })
    } else {
        return res.status(500).send({
            data: null,
            meta: {
                success: "false",
                msg: "获取菜品列表失败"
            }
        })
    }
}