// 导入评论集合构造函数
const { flatMap } = require('lodash')
const { Comments } = require('../../../model/comment.js')

module.exports = async (req, res, next) => {
    let commentInfo = await Comments.findOne({userId: req.body.userId, foodId: req.body.foodId})
    if(commentInfo){
        return res.status(501).send({ success: false, msg: '您对该菜品已完成评价'})
    }
    // 添加评论
    Comments.create(req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '评论成功' })
    }).catch(err => {
        return next(JSON.stringify({ message: err.message }))
    })
}