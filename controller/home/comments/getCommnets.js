const { Comments } = require('../../../model/comment.js')

module.exports = async (req, res) => {
  //接收客户端传递过来的当前页参数
  let pagenum = req.body.pagenum || 1;
  //每一页显示的数据条数
  let pagesize = req.body.pagesize;
  //页码对应的数据查询开始位置
  let start = (pagenum - 1) * pagesize;
  //查询评论数据的总数
  let count = await Comments.countDocuments({foodId: req.body.foodId});
  //总页数 Math.ceil() 向上取整
  // let total = Math.ceil(count / pagesize);
  let foodComments = await Comments.find({foodId: req.body.foodId}).populate('userId').limit(pagesize).skip(start).exec()
  if(foodComments) {
    return res.status(200).send({foodComments: foodComments, total: count, meta: { success: "true", msg: "获取评论信息成功" }})
  } else {
    return res.status(500).send({
      data: null,
      meta: {
        success: "false",
        msg: "获取评论信息失败"
      }
    })
  }
}