//引入用户集合模块
const { User, validateUser } = require('../model/user.js');
//引入加密模块bcrypt
const bcrypt = require('bcrypt');
module.exports = async (req, res, next) => {
    try {
        await validateUser(req.body)
    } catch (error) {
    //    return res.status(501).send({ success: 'false', msg: '验证失败，请检查输入信息' })
    return next(JSON.stringify({ message: error.message }))
    }
    //根据手机号码查询用户是否存在
    let userInfo = await User.findOne({ tel: req.body.tel });
    if (userInfo) {
        //如果手机号码已被注册
        // return res.status(502).send({ success: 'false', msg: '该手机号已被注册', user: userInfo })
        return next(JSON.stringify({success: 'false', msg: '该手机号已被注册'}))
    }
    // 对密码进行加密处理 生成随机字符串
    let salt = await bcrypt.genSalt(10);
    // 密码加密
    let saltpw = await bcrypt.hash(req.body.password, salt);
    // 密码替换
    req.body.password = saltpw;
    //将用户信息添加到数据库
    User.create(req.body).then(() => {
        return res.status(200).send({ success: 'true', msg: '添加成功' })
    }).catch(err => {
        return next(JSON.stringify({ message: err.message }))
        // return res.status(505).send({ success: 'false', msg: '添加失败', err: err })
    })
}